library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity blakley_tb is
end;

architecture bench of blakley_tb is

    -----------------------------------------------------------------------------
    --  Signal declarations
    -----------------------------------------------------------------------------

    -- Interfacing for Blakley
    signal a_in              : std_logic_vector (255 downto 0);
    signal b_in              : std_logic_vector (255 downto 0);
    signal n_in              : std_logic_vector (255 downto 0);
    signal clk               : std_logic;
    signal reset_n           : std_logic;
    signal r_out             : std_logic_vector (255 downto 0);
    signal data_in_valid     : std_logic;
    signal data_in_ready     : std_logic;
    signal data_out_valid    : std_logic;
    signal data_out_ready    : std_logic;

    constant clock_period    : time := 10 ns;
    signal stop_the_clock    : boolean;

    -- Debugging signals
    signal test_counter_s    : integer;



    -----------------------------------------------------------------------------
    --  Procedures
    -----------------------------------------------------------------------------


    begin

        i_blakley                  : entity work.blakley
           port map ( a_in           => a_in,
                      b_in           => b_in,
                      n_in           => n_in,
                      clk            => clk,
                      reset_n        => reset_n,
                      r_out          => r_out,
                      data_in_valid  => data_in_valid,
                      data_in_ready  => data_in_ready,
                      data_out_valid => data_out_valid,
                      data_out_ready => data_out_ready );

        run_tests : process (clk, reset_n) is

        -- Define test variables
        type test_values is array (0 to 9) of integer;
        variable a_test          : test_values := (104, 350, 96, 45, 6, 220, 280, 97, 16, 87);
        variable b_test          : test_values := (245, 226, 34, 33, 6, 189, 96, 225, 75, 155);
        variable n_test          : test_values := (291, 441, 101, 50, 11, 308, 284, 277, 140, 284);
        variable r_test          : test_values := (163, 161, 32, 35, 3, 0, 184, 219, 80, 137);

        variable number_of_tests : integer := 10;
        variable test_counter    : integer := 0;


    begin

        -- For debugging
        test_counter_s <= test_counter;


        -- Reset all registers async
        if reset_n = '0' then
            a_in <= (others => '0');
            b_in <= (others => '0');
            n_in <= (others => '0');

            data_in_valid <= '0';
            data_out_ready <= '0';

        -- Run test bench
        elsif rising_edge(clk) then

            -- Return results and validate
            if data_out_ready = '1' and data_out_valid = '1' then

                data_out_ready <= '0';

                -- Load result
                assert r_out = std_logic_vector(to_unsigned(r_test(test_counter), r_out'length));
                             report "********************************************************************************";
                             report "Blakley failed for test case = " & INTEGER'IMAGE(test_counter);
                             report "********************************************************************************";
                             report "Ending simulation" severity warning;

                 -- Increment test counter or exit the testbench
                  if test_counter < number_of_tests then
                     report "********************************************************************************";
                     report "Blakley passed for test case = " & INTEGER'IMAGE(test_counter);
                     report "********************************************************************************";
                     test_counter := test_counter + 1;
                  else
                      stop_the_clock <= True;
                      report "********************************************************************************";
                      report "All tests passed!" severity note;
                      report "********************************************************************************";
                  end if;

              -- Data transfer done. Wait for result.
              elsif data_in_ready = '1' and data_in_valid = '1' then
                  data_in_valid <= '0';
                  data_out_ready <= '1';

              -- Prpeate input data for Blakley
              elsif data_in_valid = '0' and data_out_ready = '0' then
                  a_in     <= std_logic_vector(to_unsigned(a_test(test_counter), a_in'length));
                  b_in     <= std_logic_vector(to_unsigned(b_test(test_counter), b_in'length));
                  n_in     <= std_logic_vector(to_unsigned(n_test(test_counter), n_in'length));

                  data_in_valid <= '1';

              end if;
          end if;


    end process;


-----------------------------------------------------------------------------
--  Running them clocks baby
-----------------------------------------------------------------------------

    clocking : process
    begin
        while not stop_the_clock loop
            clk <= '0', '1' after clock_period / 2;
            wait for clock_period;
        end loop;
        wait;
    end process;

    -- reset_n generator
    reset_gen: process
    begin
        reset_n <= '0';
        wait for 20 ns;
        reset_n <= '1';
        wait;
    end process;

end bench;
