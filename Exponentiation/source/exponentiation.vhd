library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity exponentiation is
    generic (
		C_block_size : integer := 256;
        C_number_of_cores : integer := 3
	);

    port (
		--input control
		valid_in : in std_logic;
		ready_in : out std_logic;

		--input data
		message : in std_logic_vector (C_block_size - 1 downto 0);
		key : in std_logic_vector (C_block_size - 1 downto 0);

		--output control
		ready_out : in std_logic;
		valid_out : out std_logic;

		--output data
		result : out std_logic_vector(C_block_size - 1 downto 0);

		--modulus
		modulus : in std_logic_vector(C_block_size - 1 downto 0);

		--utility
		clk : in std_logic;
		reset_n : in std_logic;

        msgin_last : in std_logic;
        msgout_last : out std_logic
	);

end exponentiation;


architecture expBehave of exponentiation is

    -- Core FIFO
	signal core_valid_in : std_logic_vector (C_number_of_cores - 1 downto 0);
	signal core_ready_in : std_logic_vector (C_number_of_cores - 1 downto 0);
	signal core_ready_out : std_logic_vector (C_number_of_cores - 1 downto 0);
	signal core_valid_out : std_logic_vector (C_number_of_cores - 1 downto 0);

    -- States for state machine
    type states_type is (start_cores, load_cores, idle);
    signal state_s : states_type;
    signal core_counter : integer range 0 to C_number_of_cores - 1;

begin

    generate_cores: for core_number in 0 to (C_number_of_cores - 1) generate
    init_core: entity work.binary
		generic map (
			C_block_size => C_BLOCK_SIZE
		)
		port map (
			message   => message,
			key       => key,

			valid_in => core_valid_in(core_number),
			ready_in => core_ready_in(core_number),
			ready_out => core_ready_out(core_number),
			valid_out => core_valid_out(core_number),

			result    => result,
			modulus   => modulus,

			clk       => clk,
			reset_n   => reset_n
		);
    end generate;



    sync: process (clk, reset_n)

        variable state : states_type;

    begin

        state_s <= state; -- for debugging

        if reset_n = '0' then

            -- Reset state machine
            state := start_cores;
            core_counter <= 0;

        -- State machine for handling cores
		elsif rising_edge(clk) then

            -- Prepare circuit and load data
			if state = start_cores then

                -- Transfer data to core and increment to next core
                if core_ready_in(core_counter) = '1' and valid_in = '1' then

                    if core_counter = ( C_number_of_cores - 1) then
                        state := load_cores;
                        core_counter <= 0;
                    else
                        core_counter <= core_counter + 1;
                    end if;
                end if;

            -- Load processed data from cores
            elsif state = load_cores then

                if core_valid_out(core_counter) = '1' and ready_out = '1' then

                    if core_counter = (C_number_of_cores - 1) then
                        state := start_cores;
                        core_counter <= 0;
                    else
                        core_counter <= core_counter + 1;
                    end if;
                end if;

            -- Wait for new data
            elsif state = idle then
                if msgin_last = '0' then
                    state := start_cores;
                end if;

            end if;

        end if;

    end process;

    -- Map FIFO signals to and from the cores using the respective busses
    async: process(valid_in, core_ready_in, ready_out, core_valid_out, core_counter)

    begin
        core_valid_in <= (core_counter => valid_in, others => '0');
        ready_in <= core_ready_in(core_counter);

        core_ready_out <= (core_counter => ready_out, others => '0');
        valid_out <= core_valid_out(core_counter);

    end process;
    
    msgout_last <= msgin_last; -- TODO: Fix this later

end architecture;
