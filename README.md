# RSA Hardware Accelerator

RSA Hardware Accelerator for FPGA designed as part of the course TFE4141 Design of digital systems. Created by Peter Uran and Eivind Jølsgaard. Further expanded upon by Peter Uran for multi-core support.